import sys
import time
import numpy as np
from functools import reduce

from parse_instance import parse_jena_file, parse_falkenauer_file


class BPP:
    def __init__(self, capacity):
        self.capacity = capacity
        self.items_per_bin = [[]]
        self.weight_per_bin = [0]

    def first_fit(self, item, bins, weights):
        for b in range(len(bins)):
            if weights[b] + item <= self.capacity:
                return b

        return None

    def fill_bins_first_fit(self, items, bins=[], weights=[]):
        items = sorted(items, reverse=True)
        for item in items:
            ff_bin = self.first_fit(item, bins, weights)
            if ff_bin is not None:
                bins[ff_bin].append(item)
                weights[ff_bin] += item
            else:
                bins.append([item])
                weights.append(item)
        
        return bins, weights

    def ruin_recreate(self, B):
        # sort by "emptyness"
        sorted_solution =  sorted(zip(self.items_per_bin, self.weight_per_bin), key = lambda x : x[1])

        # open B most empty bins
        free_items = [item for b, weight in sorted_solution[:B] for item in b]

        def replace(b, weight):
            n = len(b)
            m = len(free_items)

            permutations = [ ([i, j], [b[i], b[j]]) for i in range(n) for j in range(i+1, n) ]
            permutations += [ ([i], [b[i]]) for i in range(n) ]

            free_permutations = [ ([i, j], [free_items[i], free_items[j]]) for i in range(m) for j in range(i+1, m) ]
            free_permutations += [ ([i], [free_items[i]]) for i in range(m) ]

            for items in permutations:
                for fitems in free_permutations:
                    sum_items, sum_fitems = sum(items[1]), sum(fitems[1])
                    if sum_fitems > sum_items and weight - sum_items + sum_fitems < self.capacity:
                        for i in reversed(items[0]):
                            del b[i]
                        for i in reversed(fitems[0]):
                            del free_items[i]
                        b.extend(fitems[1])
                        free_items.extend(items[1])

                        return b, weight - sum_items + sum_fitems
            
            return b, weight

        repacked_bins = []
        weights = []
        for b, weight in sorted_solution[B:]:
            new_b, new_weight = replace(b, weight)
            repacked_bins.append(new_b)
            weights.append(new_weight)

        return self.fill_bins_first_fit(free_items, repacked_bins, weights)

    def iterated_local_search(self, B = 4):
        # 1 - sum(fullness / C)^2 / n
        def fitness(S):
            n = len(S)
            return 1.0 - sum(map(lambda b: (float(sum(b)) / self.capacity)**2.0, S)) / n

        improvement = True

        while improvement:
            fs = fitness(self.items_per_bin)
            new_solution, new_weights = self.ruin_recreate(B)
            improvement = fitness(new_solution) < fs
            if improvement:
                self.items_per_bin = new_solution
                self.weight_per_bin = new_weights

        return self.items_per_bin

    def best_fit(self, item):
        # TODO use AVL tree to become O(nlogn) instead of O(n^2).
        best_bin, best_remainder = None, float('inf')
        for b in range(len(self.items_per_bin)):
            remainder = self.capacity - self.weight_per_bin[b] - item
            if (remainder >= 0) & (remainder < best_remainder):
                best_bin, best_remainder = b, remainder
        return best_bin


    def best_swap_candidate(self, origin, origin_item, new_item, target):
        '''
        Finds the best item from the target bin that can be swapped with origin_item.
        The best item is the largest one, i.e., it leaves the most empty space in the target bin. 
        Target loses target_item and gets origin_item, and origin loses origin_item and gets target_item and new_item.
        With the swap, new_item and target_item must fit in the origin bin, and origin_item must fit in the target bin.
        '''
        empty_space = self.capacity - self.weight_per_bin[origin]
        # The space left if we remove the largest item from origin and add new_item.
        capacity_left = empty_space + origin_item - new_item

        # Stop here if new_item wouldn't fit in origin even if we swapped its largest item with nothing.
        if capacity_left <= 0:
            return None

        best_item = float('-inf')
        for target_item in self.items_per_bin[target]:
            # new_item and target_item don't fit origin or origin_item doesn't fit target.
            if target_item > capacity_left or origin_item > self.capacity - self.weight_per_bin[target] + target_item:
                continue
            # We consider the best item to be the largest one that fits the bin, i.e., 
            # the one that leaves the most empty space in the target bin. 
            # TODO could also be the one that leaves the most empty space in the origin bin.
            best_item = max(best_item, target_item)

        # No item fits.
        if best_item == float('-inf'):
            return None

        return best_item

    def swap(self, bin1, item1, bin2, item2):
        self.items_per_bin[bin1].remove(item1)
        self.items_per_bin[bin1].append(item2)
        self.items_per_bin[bin2].remove(item2)
        self.items_per_bin[bin2].append(item1)
        self.weight_per_bin[bin1] += item2 - item1
        self.weight_per_bin[bin2] += item1 - item2

    def one_opt_first_improvement(self, new_item, origin):
        '''
        Tries to swap the largest item from the origin bin with the best item from the first bin.
        Returns True is the swap was successfully made.
        '''
        # Tries to swap the largest item in the origin bin
        origin_item = max(self.items_per_bin[origin]) 

        # Finds the first item from another bin that can be swapped with origin_item.
        for target in range(len(self.items_per_bin)):
            if target != origin:
                best_item = self.best_swap_candidate(origin, origin_item, new_item, target)
                if best_item is not None:
                    self.swap(origin, origin_item, target, best_item)
                    return True

        return False

    def one_opt_best_improvement(self, new_item, origin):
        '''
        Tries to swap the largest item from the origin bin with the best item from another bin.
        Returns True is the swap was successfully made.
        '''
        # TODO: tentar trocar o menor item viável ao invés do maior?
        origin_item = max(self.items_per_bin[origin])

        best_bin, best_item = None, None
        smallest_new_weight = float('inf')

        # Finds the best item from another bin to be swapped with origin_item.
        for target in range(len(self.items_per_bin)):
            if target != origin:
                target_item = self.best_swap_candidate(origin, origin_item, new_item, target) 
                if target_item is not None and self.weight_per_bin[target] - target_item + new_item < smallest_new_weight:
                    best_bin, best_item = target, target_item
                    smallest_new_weight = self.weight_per_bin[target] - target_item + new_item

        if best_bin is not None:
            self.swap(origin, origin_item, best_bin, best_item)
            return True
        else:
            return False

    def add_item_one_opt(self, item):
        bf_bin = self.best_fit(item)

        if bf_bin is not None:
            self.items_per_bin[bf_bin].append(item)
            self.weight_per_bin[bf_bin] += item
        else:
            most_empty = np.argmin(self.weight_per_bin)
            improved = self.one_opt_best_improvement(item, most_empty)
            if improved:
                self.items_per_bin[most_empty].append(item)
                self.weight_per_bin[most_empty] += item
            else:
                self.items_per_bin.append([item])
                self.weight_per_bin.append(item)
    
    def add_item_best_fit(self, item):
        bf_bin = self.best_fit(item)

        if bf_bin is not None:
            self.items_per_bin[bf_bin].append(item)
            self.weight_per_bin[bf_bin] += item
        else:
            self.items_per_bin.append([item])
            self.weight_per_bin.append(item)

    def solve(self, item_stream, heuristic):
        self.items_per_bin = [[]]
        self.weight_per_bin = [0]

        if heuristic == 'bf':
            add_item = self.add_item_best_fit 
        elif heuristic == '1opt':
            add_item = self.add_item_one_opt
        else:
            raise "Unkown heuristic"

        # item_stream.sort(reverse=True)
        for item in item_stream:
            add_item(item)

        return self.items_per_bin


if __name__ == "__main__":
    instances = parse_falkenauer_file('Dataset/binpack3.txt')

    for  _, capacity, items, opt in instances:
        bpp = BPP(capacity)
        t1 = time.time()
        bins_bf, _ = bpp.solve(items, '1opt')
        t2 = time.time()

        t3 = time.time()
        bins_one_opt, _ = bpp.solve(items, 'bf')
        t4 = time.time()
        
        print(len(bins_bf), len(bins_one_opt), opt, t4-t3, t2-t1)

