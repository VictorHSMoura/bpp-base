from one_opt import BPP
from parse_instance import parse_falkenauer_file, parse_jena_file
import argparse
from os import path

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Read Bin Packing input.')
    parser.add_argument('-f', dest='input_file', action='store', required=True,
                        type=str, help='Input file for BPP.')
    parser.add_argument('-d', dest='dataset', action='store', required=True,
                        type=str, help='Dataset of the input file.', choices=['falkenauer', 'jena'])
    parser.add_argument('-a', dest='alg', action='store', required=True,
                        type=str, help='The algorithm to be used.', choices=['bf', '1opt'])
    parser.add_argument('-ils', dest='ils', action='store_true', 
                         help='Whether to perform iterated local search after the first algorithm.')
                        
    args = parser.parse_args()

    if not path.exists(args.input_file):
        print(f"Input file doesn't exist: {args.input_file}")
        exit(1)

    if args.dataset == 'falkenauer':
        instances = parse_falkenauer_file(args.input_file)
        print('BINS\tOPT')
        print('----\t---')
        for _, capacity, items, opt in instances:
            bpp = BPP(capacity)
            items = bpp.solve(items, args.alg)

            if args.ils:
                items = bpp.iterated_local_search()

            print(f"{len(items)}\t{opt}")

    elif args.dataset == 'jena':
        _, capacity, items = parse_jena_file(args.input_file)
        bpp = BPP(capacity)
        items = bpp.solve(items, args.alg)

        if args.ils:
            items = bpp.iterated_local_search()

        print(f"{len(items)} bins")
