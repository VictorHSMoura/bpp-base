def remove_new_line_char(lines):
    return [line[:-1] for line in lines if line[-1] == "\n"]


def parse_jena_file(filename: str):
    with open(filename, 'r') as f:
        lines = remove_new_line_char(f.readlines())

    n_items = int(lines[0])
    capacity = int(lines[1])
    items = []
    for i in range(2, len(lines)):
        items.append(int(lines[i]))

    return n_items, capacity, items

def parse_falkenauer_file(filename: str):
    with open(filename, 'r') as f:
        lines = remove_new_line_char(f.readlines())

    n_instances = int(lines[0])
    l = 1
    instances = []
    for _ in range(n_instances):
        capacity, n_items, opt = lines[l+1].split()
        capacity, n_items, opt = float(capacity), int(n_items), int(opt)
        l += 2

        items = []
        for i in range(l, l+n_items):
            items.append(float(lines[i]))
        l += n_items
        instances.append((n_items, capacity, items, opt))
    
    return instances